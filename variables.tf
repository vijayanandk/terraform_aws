# Input variable definitions

variable "vpc_name" {
  description = "terraform Provisioned VPC"
  type        = string
  default     = "tf-vpc"
}

variable "vpc_cidr" {
  description = "TF CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_azs" {
  description = "TF Availability zones for VPC"
  type        = list
  default     = ["us-west-2a", "us-west-2b", "us-west-2c"]
}

variable "vpc_private_subnets" {
  description = "TF Private subnets for VPC"
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "vpc_public_subnets" {
  description = "TF Public subnets for VPC"
  type        = list(string)
  default     = ["10.0.101.0/24", "10.0.102.0/24"]
}

variable "vpc_enable_nat_gateway" {
  description = "TF Enable NAT gateway for VPC"
  type    = bool
  default = true
}

variable "vpc_tags" {
  description = "TF Provisioned"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "dev"
  }
}
